public with sharing class CaseTriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public CaseTriggerHandler(boolean isExecuting, integer size){
        this.m_isExecuting = isExecuting;
        this.BatchSize = size;
    }

    //public void OnBeforeInsert(Case[] newRecords){} 
    
    public void OnAfterInsert(Case[] newRecords){
        Set<Id> setContactIds = new Set<Id>();
        for(Case caseItem : newRecords){
           if(caseItem.ContactId != null) setContactIds.add(caseItem.ContactId);
        }
        if(setContactIds.size() > 0){
            List<Task> listTask2Insert = new List<Task>();
            Map<Id, Contact> map_ContactId_Contact = new Map<Id, Contact>([	SELECT Id, Name, OwnerId 
                                                                            FROM Contact 
                                                                            WHERE Id IN : setContactIds]);
            for(Case caseItem: newRecords){
                Contact theContact;
                if(caseItem.ContactId != null) theContact = map_ContactId_Contact.get(caseItem.ContactId);
                Task newTask = TaskUtilities.createTaskWithCaseContact(caseItem, theContact);
                listTask2Insert.add(newTask);
            }
            if(listTask2Insert.size()>0){
                insert listTask2Insert;
            }
        }
        setContactIds.clear();
    }
	/*
    @future public static void OnAfterInsertAsync(Set<ID> newRecordIDs){}

    public void OnBeforeUpdate(Case[] oldRecords, Case[] updatedRecords, Map<ID, Case> mapUpdatedRecords){}

    public void OnAfterUpdate(Case[] oldRecords, Case[] updatedRecords, Map<ID, Case> mapUpdatedRecords){}
    
    @future public static void OnAfterUpdateAsync(Set<ID> updatedRecordIDs){}

    public void OnBeforeDelete(Case[] recordsToDelete, Map<ID, Case> mapDeletedRecords){}

    public void OnAfterDelete(Case[] deletedRecords, Map<ID, Case> mapDdeletedRecords){}

    @future public static void OnAfterDeleteAsync(Set<ID> deletedRecordIDs){}

    public void OnUndelete(Case[] restoredRecords){}

    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }

    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }
	*/
}