@isTest
public class CaseTriggerHandlerTest {
    
    static final Integer CASES_NUMBER = 200;
    static final Integer CONTACTS_NUMBER = 3; 
    public static list<Case> cases = new list<Case>();
    public static list<Contact> contacts = new list<Contact>();
    
    public static list<Case> setupCases(Integer casesNumber, list<Contact> listContacts) {
        if(cases.size() == 0){
            for(Integer i = 0; i < casesNumber; i++){
                Case newCase = new Case();
                    newCase.Status = CaseUtilities.CASE_STATUS;
                    newCase.Origin = CaseUtilities.CASE_ORIGIN;
                    Integer j = math.mod(i, 3);
                    if(listContacts[j].Id != null) {
                        newCase.ContactId = listContacts[j].Id;
                        newCase.AccountId = listContacts[j].AccountId;
                    }
                cases.add(newCase);
            }
            if(cases.size() > 0) insert cases;
        }
        return cases;
    }
    
    static testMethod void testInsertCases() {
        System.runAs(UserUtilities.setupUser('test','System Administrator'))
        {
            Test.startTest();
        		contacts = ContactTriggerHandlerTest.setupContacts(CONTACTS_NUMBER);
            	Integer countTasks  =  [SELECT Count() FROM Task  WHERE Subject LIKE 'Welcome call for%' ];
             	System.assertEquals(CONTACTS_NUMBER, countTasks, 'Quantity of created Tasks the same as quantity of created Cases (quantity of Cases = quantity of Contacts)');
            	
            	setupCases(CASES_NUMBER,contacts);
            	countTasks =  [SELECT Count() FROM Task  WHERE Subject LIKE 'Welcome call for%' ];
            	System.assertEquals((CASES_NUMBER + CONTACTS_NUMBER),countTasks, 'Quantity of created Tasks the same as quantity of created Cases');
            Test.stopTest();
        }
    }
}