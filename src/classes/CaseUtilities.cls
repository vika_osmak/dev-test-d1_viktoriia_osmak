global class CaseUtilities {
    public static final String CASE_STATUS = 'Working';
    public static final String CASE_ORIGIN = 'New Contact';
    public static final Map<String, String> MAP_CONTACTLEVEl_CASEPRIORITY = new Map<String, String>{'Primary' => 'High','Secondary' => 'Medium','Tertiary' => 'Low'};
    
    public static Case createCaseWithContact(Contact theContact){
		Case newCase = new Case();
		newCase.Status = CASE_STATUS;
		newCase.Origin = CASE_ORIGIN;
        if(theContact != null){
            newCase.ContactId = theContact.Id;
            if(theContact.AccountId != null)
                newCase.AccountId = theContact.AccountId;
            if(theContact.Level__c != null && MAP_CONTACTLEVEl_CASEPRIORITY.containsKey(theContact.Level__c))
                newCase.Priority = MAP_CONTACTLEVEl_CASEPRIORITY.get(theContact.Level__c);
            newCase.OwnerId = theContact.OwnerId;
        }
		return newCase;
    }
}