@isTest
public class CaseUtilitiesTest {
    static testMethod void testCreateTaskWithCaseContact() 
    {
        System.runAs(UserUtilities.setupUser('test','System Administrator'))
        {
        	List<Contact> contacts = ContactTriggerHandlerTest.setupContacts(3);
            
            set<Id> insertedContactIds = new set<Id>();
            for(Contact contactItem : contacts) insertedContactIds.add(contactItem.Id);
            map<Id,Contact> map_ContactId_Contact = new map<Id,Contact>( 
                				[SELECT Id,Name,Level__c,AccountId,OwnerId
                                FROM Contact 
                                WHERE Id IN : insertedContactIds]); 
            map<Id,Case> map_CaseId_Case = new map<Id,Case>(
                				[SELECT Id,ContactId,AccountId,Status,Origin,Priority,OwnerId,CaseNumber
                                FROM Case 
                                WHERE ContactId IN : insertedContactIds]);
            Test.startTest();
            	for(Id contacId :  map_ContactId_Contact.keySet()){
                    Contact theContact = map_ContactId_Contact.get(contacId);
                    Case theCase = CaseUtilities.createCaseWithContact(theContact);
					System.assertEquals(CaseUtilities.CASE_STATUS, theCase.Status);
                	System.assertEquals(CaseUtilities.CASE_ORIGIN, theCase.Origin);
                    if(theContact.Level__c == 'Primary')
                        System.assertEquals('High', theCase.Priority);
                    if(theContact.Level__c == 'Secondary')
                        System.assertEquals('Medium', theCase.Priority);
                    if(theContact.Level__c == 'Tertiary')
                    	System.assertEquals('Low', theCase.Priority);
                    System.assertEquals(theContact.OwnerId, theCase.OwnerId, 'Case Owner should be the same as Contact Owner');
                }
        	Test.stopTest();
        }
    }
}