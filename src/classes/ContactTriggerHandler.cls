public with sharing class ContactTriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public ContactTriggerHandler(boolean isExecuting, integer size){
        this.m_isExecuting = isExecuting;
        this.BatchSize = size;
    }

    public void OnBeforeInsert(Contact[] newRecords){}
    
    public void OnAfterInsert(Contact[] newRecords){
        List<Case> listCases2Insert = new List<Case>();
        for(Contact contactItem : newRecords){
            Case newCase = CaseUtilities.createCaseWithContact(contactItem);
			listCases2Insert.add(newCase);
        }
        if(listCases2Insert.size() > 0){
            insert listCases2Insert;
            listCases2Insert.clear();
        }
    }
	/*
    @future public static void OnAfterInsertAsync(Set<ID> newRecordIDs){}

    public void OnBeforeUpdate(Contact[] oldRecords, Contact[] updatedRecords, Map<ID, Contact> mapUpdatedRecords){}

    public void OnAfterUpdate(Contact[] oldRecords, Contact[] updatedRecords, Map<ID, Contact> mapUpdatedRecords){}
    
    @future public static void OnAfterUpdateAsync(Set<ID> updatedRecordIDs){}

    public void OnBeforeDelete(Contact[] recordsToDelete, Map<ID, Contact> mapDeletedRecords){}

    public void OnAfterDelete(Contact[] deletedRecords, Map<ID, Contact> mapDdeletedRecords){}

    @future public static void OnAfterDeleteAsync(Set<ID> deletedRecordIDs){}

    public void OnUndelete(Contact[] restoredRecords){}
    

    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }

    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }
    */
}