@isTest
public class ContactTriggerHandlerTest {
    
    static final Integer CONTACTS_NUMBER = 200;
    public static Account newAccount;    
    public static list<Contact> contacts = new list<Contact>();

    public static Account setupAccount() {
        if(newAccount == null){
			newAccount = new Account(Name = 'TestAccount');
			insert newAccount;
    	}
        return newAccount;
    }
    
    public static list<Contact> setupContacts(Integer contactsNumber) {
        if(contacts.size() == 0){
        	User testSTUser = UserUtilities.setupUser('vika','Standard User');
            newAccount = setupAccount();
        	for(Integer i = 0; i < contactsNumber; i++){
				Contact newContact = new Contact();
                newContact.LastName = 'TestContact' + i;
                if(i != 0 && newAccount!= null) newContact.AccountId = newAccount.Id; // contacts[0].AccountId = null does not have linked Account, others have linked Account (contacts[i].newContact.AccountId != null)    
				if(math.mod(i, 3) == 0) newContact.Level__c = 'Primary';
                else if(math.mod(i, 3) == 1) newContact.Level__c = 'Secondary';
                else if(math.mod(i, 3) == 2) newContact.Level__c = 'Tertiary';
                newContact.OwnerId = testSTUser.Id;
                contacts.add(newContact);
        	}
        	insert contacts;
        }
        return contacts;
    }
    
    static testMethod void testInsetrtContacts() 
    {
        System.runAs(UserUtilities.setupUser('test','System Administrator'))
        {
            Test.startTest();
                setupAccount();
        		setupContacts(CONTACTS_NUMBER);
            Test.stopTest();
            set<Id> insertedContactIds = new set<Id>();
            for(Contact contactItem : contacts) insertedContactIds.add(contactItem.Id);
            map<Id,Case> map_ContactId_Case = new map<Id,Case>();
            for(Case caseItem: [SELECT Id,ContactId,AccountId,Status,Origin,Priority,OwnerId
                                FROM Case 
                                WHERE ContactId IN : insertedContactIds]){
                map_ContactId_Case.put(caseItem.ContactId, caseItem);
            }
            System.assertEquals(CONTACTS_NUMBER, map_ContactId_Case.values().size(), 'Quantity of created Cases the same as quantity of created Contacts');
            for(Integer i = 0; i < CONTACTS_NUMBER; i++){
                Case newCase = map_ContactId_Case.get(contacts[i].Id);
                System.assertEquals(CaseUtilities.CASE_STATUS, newCase.Status);
                System.assertEquals(CaseUtilities.CASE_ORIGIN, newCase.Origin);
                if(i == 0) 
                    System.assertEquals(null, newCase.AccountId, 'Contact contacts[0] does not have linked Account, Case should not be linked to Account');
                else 
                    System.assertEquals(contacts[i].AccountId, newCase.AccountId, 'Contact contacts[1] has linked Account, Case should be linked to the same Account');
                if(math.mod(i, 3) == 0)
                    System.assertEquals('High', newCase.Priority, 'Case Priority should be equal High If Conact.Level__c = Primary');
                if(math.mod(i, 3) == 1)
                    System.assertEquals('Medium', newCase.Priority, 'Case Priority should be equal Medium If Conact.Level__c = Secondary');
                if(math.mod(i, 3) == 2)
                    System.assertEquals('Low', newCase.Priority, 'Case Priority should be equal Low If Conact.Level__c = Tertiary');
                System.assertEquals(contacts[i].OwnerId, newCase.OwnerId, 'Case Owner should be the same as Contact Owner');
            }
        }
    }
}