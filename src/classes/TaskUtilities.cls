global class TaskUtilities {
	public static final String TASK_PRIORITY = 'Normal';
    public static final Map<String, Integer> MAP_CASEPRIORITY_TASKWEEKS = new Map<String, Integer> {'High' => 1,'Medium' => 2,'Low' => 3};
	
	public static Task createTaskWithCaseContact(Case theCase, Contact theContact)
    {
        Task newTask = new Task();
        newTask.Priority = TASK_PRIORITY;
        newTask.Subject = 'Welcome call for ';
        if(theContact != null) {
            newTask.Subject += theContact.Name + ' - ';
            newTask.OwnerId = theContact.OwnerId;
        }
        if(theCase != null){
            newTask.Subject += theCase.CaseNumber;
            if(theCase.Priority != null && MAP_CASEPRIORITY_TASKWEEKS.containsKey(theCase.Priority)){
                newTask.ActivityDate = Date.today().addDays(7 * MAP_CASEPRIORITY_TASKWEEKS.get(theCase.Priority));
            }
		}
        return newTask;
    }
}