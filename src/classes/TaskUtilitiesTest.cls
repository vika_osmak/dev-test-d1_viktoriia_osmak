@isTest
public class TaskUtilitiesTest {
    static testMethod void testCreateTaskWithCaseContact() 
    {
        System.runAs(UserUtilities.setupUser('test','System Administrator'))
        {
        	List<Contact> contacts = ContactTriggerHandlerTest.setupContacts(3);
            
            set<Id> insertedContactIds = new set<Id>();
            for(Contact contactItem : contacts) insertedContactIds.add(contactItem.Id);
            map<Id,Contact> map_ContactId_Contact = new map<Id,Contact>( 
                				[SELECT Id,Name,Level__c,AccountId,OwnerId
                                FROM Contact 
                                WHERE Id IN : insertedContactIds]); 
            map<Id,Case> map_CaseId_Case = new map<Id,Case>(
                				[SELECT Id,ContactId,AccountId,Status,Origin,Priority,OwnerId,CaseNumber
                                FROM Case 
                                WHERE ContactId IN : insertedContactIds]);
            Test.startTest();
            	for(Id caseId : map_CaseId_Case.keySet()){
                    Case theCase = map_CaseId_Case.get(caseId);
                    Contact theContact = map_ContactId_Contact.get(theCase.ContactId);
                    Task newTask = TaskUtilities.createTaskWithCaseContact(theCase, theContact);
                    
                    System.assertEquals(TaskUtilities.TASK_PRIORITY, newTask.Priority);
                    System.assertEquals('Welcome call for ' + theContact.Name + ' - '+theCase.CaseNumber, newTask.Subject, 'Task Subject - “Welcome call for {contact name} - {case number}”');
                    Integer dueDate = Date.today().daysBetween(newTask.ActivityDate);
                    if(theContact.Level__c == 'Primary')
                    {
                        System.assertEquals('High', theCase.Priority);
                        System.assertEquals(1 * 7, dueDate, 'Task due date should be equal 1 week  If Case Priority = High');
                    }
                    if(theContact.Level__c == 'Secondary')
                    {
                        System.assertEquals('Medium', theCase.Priority);
                        System.assertEquals(2 * 7, dueDate, 'Task due date should be equal 2 weeks If Case Priority = Medium');
                    }
                    if(theContact.Level__c == 'Tertiary')
                    {
                        System.assertEquals('Low', theCase.Priority);
                        System.assertEquals(3 * 7, dueDate, 'Task due date should be equal 3 weeks If Case Priority = Low');
                    }
                    System.assertEquals(theCase.OwnerId, newTask.OwnerId, 'Task owner the same as the user who owns the contact');
                }
        	Test.stopTest();
        }
    }
}