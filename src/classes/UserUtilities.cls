global class UserUtilities {
    public static User setupUser(String userIndt, String profileName) {
        userIndt += Integer.valueOf(Math.rint(math.random()*1000000));
        User newUser = new User();
        	newUser.UserName =  userIndt + '@mytest.com'; 
            newUser.FirstName = userIndt + 'TestFirstName';
            newUser.LastName =  userIndt + 'TestLastName';
            newUser.Email =  userIndt + '@mytest.com';
            newUser.EmailEncodingKey = 'UTF-8';
            newUser.Alias =  'testA';
            newUser.TimeZoneSidKey = 'Europe/London';
        	newUser.LocaleSidKey = 'en_GB';
        	newUser.LanguageLocaleKey = 'en_US';
        	newUser.ProfileId = [SELECT Id FROM Profile WHERE Name = : profileName].Id;
            insert newUser;
        return  newUser;
    }
}